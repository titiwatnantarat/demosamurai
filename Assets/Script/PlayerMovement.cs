using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float speed = 1f;
    public float jumpSpeed = 9f;
    public float maxSpeed = 10f;
    public float JumpPower = 20f;
    public bool ground;
    public float jumpRate = 1f;
    public float nextJumpPress = 0.0f;
    public float fireRate = 0.2f;
    public float nextFire = 0.0f;
    private Rigidbody2D rigidbody2D;
    private Physics2D physics2D;
    Animator animator;
    public int healthbar = 100;
    public GameObject hitArea;
    // Start is called before the first frame update
    void Start()
    {
        rigidbody2D = this.GetComponent<Rigidbody2D>();
        animator = this.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        animator.SetBool("Ground" , true);
        animator.SetFloat("Speed" , Mathf.Abs(Input.GetAxis("Horizontal")));
        if(Input.GetAxis("Horizontal") < -0.01f)
        {
            transform.Translate(Vector2.right * speed * Time.deltaTime);
            transform.eulerAngles = new Vector2(0,188);
        } else if(Input.GetAxis("Horizontal") > 0.1f)
        {
            transform.Translate(Vector2.right * speed * Time.deltaTime);
            transform.eulerAngles = new Vector2(0,0);
        }

        if(Input.GetButtonDown("Jump") && Time.time > nextJumpPress)
        {
            animator.SetBool("Jump" , true);
            nextJumpPress = Time.time + jumpRate;
            rigidbody2D.AddForce(jumpSpeed * (Vector2.up * JumpPower));
        }else
        {
            animator.SetBool("Jump" , false);
        }

        if(Input.GetKey(KeyCode.J) && Time.time > nextFire)
        {
            nextFire = Time.time + fireRate;
            animator.SetBool("Attack" , true);
            Attack();
        }else
        {
            animator.SetBool("Attack" , false);
        }
    }

    public void Attack()
    {
        StartCoroutine(DelaySlash());
    }

    IEnumerator DelaySlash()
    {
        yield return new WaitForSeconds(0.1f);
        Instantiate(hitArea,transform.position,transform.rotation);
    }
}
